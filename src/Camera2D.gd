extends Camera2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var move_x_dir = 1 # Right
var h_amount = 0
var shaking = false
var shake_amt = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	
	pass # Replace with function body.
func _physics_process(delta):
	var spd = 1 #* abs((get_parent().velocity.x) / 200)
	if Input.is_action_pressed("left"):
		h_amount = -(spd*1.0/120.0) 
		#h_amount = h_amount*h_amount*h_amount
		offset_h += 0.2*h_amount * Input.get_action_strength("left")
	if Input.is_action_pressed("right"):
		h_amount = (spd*1.0/120.0)   
		#h_amount = h_amount*h_amount*h_amount
		offset_h += 0.2*h_amount * Input.get_action_strength("right")

	h_amount = h_amount*.985
	offset_h += h_amount
	offset_h = clamp(offset_h,-.9,.9)
	h_amount = (h_amount)
	
	var zoom_target
	if Input.is_action_pressed("zoom_out"):
		zoom_target = 4
	else:
		zoom_target = 1
	var zoom_amt = lerp(zoom.x, zoom_target, 0.1) 
	zoom = Vector2(zoom_amt, zoom_amt)
	
	
	
	# Screenshake
	if !shaking:
		shake_amt = lerp(shake_amt, 0, 0.08)
	offset = Vector2(rand_range(-shake_amt, shake_amt),rand_range(-shake_amt, shake_amt))
	
func shake(amount=4,seconds = 1):
	shaking = true
	shake_amt = amount
	yield(get_tree().create_timer(seconds), 'timeout')
	stop_shaking()

func stop_shaking():
	shaking = false
	#offset = 
	#shaking = false
	