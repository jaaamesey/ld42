extends CanvasLayer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var lives_label = get_node("Lives")  
onready var lives_number_label = get_node("LivesNumber")  
onready var original_lives_number_pos_y = lives_number_label.rect_position.y
onready var adjusted_lives_number_pos_y = lives_number_label.rect_position.y - 60

func _ready():
	
	pass # Replace with function body.

func _physics_process(delta):
	update()
	pass
func update():
	
	if get_parent().iframes <= 80: # If not in iframes
		lives_number_label.bbcode_text = str(get_parent().lives)
		lives_number_label.rect_position.y = original_lives_number_pos_y
	else: # If in iframes (just hit)
		lives_number_label.rect_position.y = adjusted_lives_number_pos_y
		lives_number_label.bbcode_text = "[b]" + str(get_parent().lives)
	
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
