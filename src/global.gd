extends Node


func _ready():
	Engine.target_fps = 180
	set_physics_process(true)
	#print("lol")
	OS.set_window_title("LD42")

var _last_time = -1
var _last_fps = 1
func _physics_process(delta):
	if floor(_last_time) != floor(OS.get_ticks_msec() / 1000) \
	and _last_fps != Engine.get_frames_per_second():
		OS.set_window_title("LD42 | FPS: "+ str(Engine.get_frames_per_second()))	
		
	_last_time = floor(OS.get_ticks_msec() / 1000)
	_last_fps = Engine.get_frames_per_second()
	