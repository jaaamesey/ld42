extends KinematicBody2D

const PlayerRigidBody2D = preload("res://src/PlayerRigidBody2D.tscn")

#onready var raycast_collision_shape = get_node("RayCastCollisionShape2D")
onready var extra_raycast = get_node("ExtraRaycast")
onready var sprite = get_node("Sprite")
onready var anim_player = get_node("AnimationPlayer")
#onready var lives_text = get_node("UI Layer/Lives")
#onready var ui_layer = get_node("UI Layer")
onready var camera = get_node("Camera2D")

var is_rigid_body = false
var rigid_body  : RigidBody2D = null

var lives = 9
var iframes = 0

const snap = Vector2(0,64)
var velocity = Vector2(0,0)
var grav_magnitude = 200
var grav_dir = Vector2(0,1).normalized()
var grav_scale = 1
var move_dir_x = 0
var x_spd = 48
#ar x_max_spd = 512
var run_spd = 1.5
var run_multiplier = 1
var additional_vector := Vector2(0,0)
var impulse_vector = Vector2(0,0)
var impulse_damp = 0.94
var jump_height = 1000
var jump_hold_influence = 26
var last_velocity = Vector2(0,0)
var default_air_control = 0.34
var air_control = default_air_control
var grounded = false
var jump_leeway_seconds = 0 #.1
var jump_held_time = 0
var player_movement := Vector2(0,0)
var rotation_angle = 0
var can_move = true
var can_jump = true
var grapple_point = Vector2(0,0)
var grapple_angle = 0
var grapple_force = 0

var state = STATE.walking
var floor_normal = Vector2(0,0)

var left_stick_vector = Vector2(0,0)


#var just_left_ground = true

func _ready():
	state = idle
	
	
	pass # Replace with function body.

		

enum STATE {
	idle,
	walking,
	in_air,
	swinging
	}
	
	
#warning-ignore:unused_argument
func _physics_process(delta):
	var input_vec = Vector2()
	input_vec.x = Input.get_action_strength("right") - Input.get_action_strength("left")
	
	
	
#warning-ignore:unused_variable
	air_control = default_air_control
	
	if !is_rigid_body:
		state = idle
	
	var floor_normal_normalized = (floor_normal).normalized()
	
	# Get collision normals
	var collision_normals = []
	var collision_0_normal = null
	for i in range(0, get_slide_count()):
		var normal = get_slide_collision(get_slide_count() - 1).normal
		collision_normals.append(normal)
		if i == 0:
			collision_0_normal = normal
	
	# Grounding check
	grounded = false
	if collision_0_normal != null and collision_0_normal.y == -grav_dir.y  or get_slide_count() >= 2\
	or extra_raycast.is_colliding():
		grounded = true
		impulse_vector.y = 0
		if !extra_raycast.is_colliding():
			grav_scale = 0
	
	if collision_0_normal != null and extra_raycast.is_colliding():
		if grav_scale < 5:
			grav_scale = 5
	
	# Jumping
	
	jump_leeway_seconds -= 1.0/60.0
	if Input.is_action_just_pressed("jump"):
		jump_leeway_seconds = 0.1
	
	if Input.is_action_pressed("jump") and grounded and can_jump and jump_leeway_seconds > 0:
		#jump_leeway_seconds = 0.1
		grounded = false
		jump_held_time = 0
		var jump_dir_x = 0 #floor_normal_normalized.x*0.01
		add_impulse(jump_height* Vector2(jump_dir_x, -1))
		grav_scale = 0
		
	elif grav_scale < 0.1:
		grav_scale = 1
		
	if !Input.is_action_pressed("jump"):
		jump_leeway_seconds = 0
		jump_held_time = -1
	if grounded:
		jump_held_time = 0
	elif jump_held_time != -1: 
		jump_held_time += 1.0/60.0
		if jump_held_time < 1:
			add_impulse(Vector2(0,-jump_hold_influence))
	
	# Input 
	var left = Input.get_action_strength("left")
	var right = Input.get_action_strength("right")
	move_dir_x = right - left
	if !can_move:
		move_dir_x = 0
		
	if move_dir_x != 0 and !is_rigid_body:
		state = walking
		
	if !grounded and  !is_rigid_body:
		state = in_air
	
	var angle_to_point = -(get_angle_to(grapple_point) + 0.5*PI) + 0.25*PI + 0.5*PI#+ 0.5*PI

		
	if Input.is_action_just_pressed("ui_cancel"):
		start_grappling()
	if Input.is_action_pressed("jump"):
		stop_grappling()
		
	
	match state:
		
		swinging:
			#start_grappling()
			swap_to_rigid_body()
			rigid_body.gravity_scale = 0
			grapple_angle = position.angle_to_point(grapple_point)
			var grapple_displacement = grapple_point - position
			air_control = 2.5
			#grav_scale = 0
			last_velocity = Vector2(0,0)
			impulse_vector = Vector2(0,0)
			additional_vector = Vector2(0,0)
			var grapple_radius = 200  #grapple_displacement.length()
			grapple_angle += -move_dir_x * 0.08
			#position.x = grapple_point.x + cos(grapple_angle)*grapple_radius
			
			# Derivative:  −r*sin(theta)
			#position.y = grapple_point.y + sin(grapple_angle)*grapple_radius
			# Derivative:  r*cos(theta)
			var spinny_vector = Vector2()
			#spinny_vector.x = -grapple_radius*sin(grapple_angle)
			#spinny_vector.y = grapple_radius*cos(grapple_angle)
			#add_velocity(100*spinny_vector.normalized())
			
			var rot_impulse_amt = 1200
			if abs(rigid_body.rotation) > 1.0: # If on bottom side
				rot_impulse_amt = 400
				pass 
			rigid_body.apply_torque_impulse(rot_impulse_amt*move_dir_x)
			
			#print(rigid_body.angular_velocity)
			
			#grapple_angle = lerp(grapple_angle, -grapple_angle, 0.01)
			#print((grapple_angle))
			#add_velocity(Vector2(0,10))
			#rigid_body.apply_central_impulse(Vector2(800*move_dir_x, 0))
			#rigid_body.apply_central_impulse(Vector2(0, 400))
			
			var length = 200
			var stretch_displacement = grapple_point - length*grapple_displacement.normalized()
			move_dir_x = 0
			#X := originX + cos(angle)*radius;
			#Y := originY + sin(angle)*radius;
#			if grapple_displacement.length() > length:
#				rigid_body.gravity_scale = 0
#				print(grapple_displacement.length())
#				rigid_body.apply_central_impulse(  800*grapple_displacement.normalized())
#				#add_velocity(2*grapple_displacement)
#				#print(grapple_displacement.length())
#			elif grapple_displacement.length() > length / 2:
#				rigid_body.apply_central_impulse( -8000*grapple_displacement.normalized())

		_: 
			swap_to_kinematic_body()	
	
	# Manage velocity
	velocity = Vector2(0,0)
	
	# Add gravity to velocity
	#if floor_normal.normalized() != Vector2(0,0):
		#grav_dir = Vector2(-floor_normal.normalized().x, -floor_normal.normalized().y)
	var gravity = grav_dir * grav_magnitude * grav_scale
	velocity += gravity
	

	
	
	# Add player movement to velocity
	if Input.is_action_pressed("run"):
		run_multiplier = run_spd
	else:
		run_multiplier = 1
	
	player_movement = x_spd*Vector2(move_dir_x, 0)*run_multiplier
	if !grounded:
		player_movement *= air_control
	
	velocity += player_movement
	
	if grounded:
		velocity.x += (last_velocity.x)*.9
	else: 
		velocity.x += (last_velocity.x)*.97
	
	# Add impulse vector to velocity
	velocity += impulse_vector
	# Add additional velocity vector to velocity
	velocity += additional_vector
	# Finally move the player
	var final_multiplier = 1.5
	if !is_rigid_body:
		last_velocity = move_and_slide_with_snap(final_multiplier*velocity, snap, -floor_normal) / (final_multiplier)
	impulse_vector *= pow(impulse_damp,.5)
	additional_vector = Vector2(0,0)
	grav_scale *= 1.03
	if grav_scale > 7:
		grav_scale = 7
	
	# Handle damage stuff
	iframes -= 1
	iframes = clamp(iframes, -1, 120)
	
	
	if is_rigid_body:
		position = rigid_body.position
		
	
	
	# Finally animate the player
	_animate()

func add_impulse(vector):
	impulse_vector += vector
	
func add_velocity(vector : Vector2):
	additional_vector += vector
	
	


func _animate():
	
	if move_dir_x > 0 and (last_velocity.x > 0 or grounded):
		sprite.flip_h = true
	if move_dir_x < 0 and (last_velocity.x < 0 or grounded):
		sprite.flip_h = false
	
	floor_normal = extra_raycast.get_collision_normal()
	# Rotate sprite to match floor normal
	var rot = 0
	if extra_raycast.is_colliding() and floor_normal.x != 0:
		rot = 90 +  rad2deg(atan2(floor_normal.y , floor_normal.x))
	else:
		rot = 0
	rotation_angle = lerp(rotation_angle,rot,0.03)
	sprite.rotation_degrees = rotation_angle
	extra_raycast.rotation_degrees = rotation_angle
	#raycast_collision_shape.rotation_degrees = rotation_angle
	#global_rotation_degrees = rotation_angle
	#print(sprite.rotation_degrees)
	
	
	# Do things based on state
	match(state):
		idle:
			sprite.speed_scale = 0
			sprite.frame = 0
			rotation = 0
		walking:
			sprite.animation = "walk"
			sprite.speed_scale = abs(player_movement.x / 20.0)
			rotation = 0
			pass
		in_air:
			sprite.speed_scale = 0
			sprite.frame = 0
			rotation = 0
			#sprite.animation = "walk"
		swinging:
			rotation = rigid_body.rotation
		_:
			state = STATE.idle
	update()

func start_grappling(point = position, starting_angular_velocity = 0):
	if state != swinging:
		position = point
		state = swinging
		swap_to_rigid_body()
		rigid_body.angular_velocity = starting_angular_velocity
func stop_grappling(keep_momentum = true):
	if state != swinging:
		return
	var angular_velocity = clamp(rigid_body.angular_velocity, -15, 15)
	var angle = rigid_body.rotation
	var direction = Vector2(cos(angle), sin(angle))
	
	swap_to_kinematic_body()
	state = idle
	if keep_momentum:
		grav_scale = 1
		# Jump
		add_impulse(Vector2(0,-(1400 + abs(angular_velocity))))
		# Side impulse
		add_impulse(8*angular_velocity*direction)
	pass	


func damage(knockback = Vector2(0,0)):
	impulse_vector.y = 0
	# Handle knockback
	var knockback_vector = knockback + Vector2(0,-1)
	grounded = false
	add_velocity(2000 * knockback_vector)
	add_impulse(Vector2(0,-800))
	
	# Do actual damage stuff
	if iframes <= 0:
		lives -= 1
		modulate = Color(1,0,0)
		anim_player.play("hurt")
		iframes = 120
		#screen_freeze(0.07)
		rumble(.7, .2)
		camera.shake(25, 0.4)
	else:
		rumble(.2, .3)
		camera.shake(10, 0.5)
		

	
	
	pass

func rumble(amount, duration):
	Input.start_joy_vibration(0, amount,amount,duration)
	pass

# On enemy hit
func _on_Hurtbox_0_body_entered(body):
	# If enemy:
	if body.is_in_group("enemy") and body.spikey \
	or body.is_in_group("hurtbox"):
		# Get direction vector
		var dir = position - body.position
		dir = dir.normalized()
		#print(dir)
		damage(dir)
	
	pass # Replace with function body.

func screen_freeze(seconds = 0.075):
	get_tree().paused = true
	yield(get_tree().create_timer(seconds), 'timeout')
	get_tree().paused = false
	

	
func swap_to_rigid_body():
	if rigid_body == null:
		grav_scale = 0
		rigid_body = PlayerRigidBody2D.instance()
		rigid_body.position = self.position
		set_collision_layer_bit(1, false)
		set_collision_mask_bit(1, false)
		add_collision_exception_with(rigid_body)
		extra_raycast.add_exception(rigid_body)
		get_parent().add_child(rigid_body)
		is_rigid_body = true

func swap_to_kinematic_body():
	if rigid_body != null:
		rigid_body.queue_free()
		rigid_body = null
		is_rigid_body = false
		set_collision_layer_bit(1, true)
		set_collision_mask_bit(1, true)
		if grav_scale < 6:
			grav_scale = 6
		
		